Source: alkimia
Section: libs
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Mark Purcell <msp@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               kf6-extra-cmake-modules,
               kf6-kcodecs-dev,
               kf6-kcompletion-dev,
               kf6-kconfig-dev,
               kf6-kcoreaddons-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-knewstuff-dev,
               kf6-kpackage-dev,
               kf6-ktextwidgets-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kxmlgui-dev,
               libgmp-dev,
               libplasma-dev,
               pkgconf,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-webengine-dev,
               xauth,
               xvfb
Standards-Version: 4.7.0
Homepage: http://kde-apps.org/content/show.php?content=137323
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/alkimia
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/alkimia.git

Package: alkimia-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Financial library common classes for KDE
 Libalkimia is a library with common classes and functionality used by
 finance applications for KDE. Currently it supports a common
 class to represent monetary values with arbitrary precision.
 .
 Support files for the library.

Package: alkimia-data
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Financial library common classes for KDE
 Libalkimia is a library with common classes and functionality used by
 finance applications for KDE. Currently it supports a common
 class to represent monetary values with arbitrary precision.
 .
 Support files for the library.

Package: libalkimia6-8
Architecture: any
Depends: alkimia-data (= ${source:Version}),
         libplasma6,
         qt6-webengine,
         ${misc:Depends}, 
         ${shlibs:Depends}
Breaks: libalkimia5-8 (<< ${source:Version}~ciBuild)
Replaces: libalkimia5-8 (<< ${source:Version}~ciBuild)
Recommends: alkimia-bin (= ${source:Version})
Description: Financial library common classes for KDE
 Libalkimia is a library with common classes and functionality used by
 finance applications for KDE. Currently it supports a common
 class to represent monetary values with arbitrary precision.

Package: libalkimia6-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: kf6-extra-cmake-modules,
         kf6-kconfig-dev,
         libalkimia6-8 (= ${binary:Version}),
         libgmp-dev,
         qt6-base-dev,
         qt6-webengine-dev,
         ${misc:Depends}
Breaks: libalkimia5-dev (<< ${source:Version}~ciBuild)
Replaces: libalkimia5-dev (<< ${source:Version}~ciBuild)
Description: Financial library - Development files
 Libalkimia is a library with common classes and functionality used by
 finance applications for the KDE SC. Currently it supports a common
 class to represent monetary values with arbitrary precision.
 .
 This package contains development files needed for Libalkimia.

Package: plasma-widget-foreigncurrencies
Architecture: any
Depends: libalkimia6-8 (= ${binary:Version}),
         qml6-module-org-kde-alkimia,
         qt6-declarative,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Financial library - Currency Plasmoid
 Libalkimia is a library with common classes and functionality used by
 finance applications for KDE. Currently it supports a common
 class to represent monetary values with arbitrary precision.
 .
 Plasma Widget for foreign currencies.

Package: qml6-module-org-kde-alkimia
Architecture: any
Depends: libalkimia6-8 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Financial library - QML Module
 Libalkimia is a library with common classes and functionality used by
 finance applications for KDE. Currently it supports a common
 class to represent monetary values with arbitrary precision.
 .
 This package contains the files for using Alkimia with QML.

Package: libalkimia-dev
Depends: libalkimia6-dev, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: libalkimia5-dev
Depends: libalkimia6-dev, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: libalkimia5-8
Depends: libalkimia6-8, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: qml-module-org-kde-alkimia
Depends: qml6-module-org-kde-alkimia, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.